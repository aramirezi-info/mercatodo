<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    function () {
        return redirect()->route('login');
    }
);

Auth::routes();
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');

Route::group(
    ['middleware' => ['auth', 'verified'], 'prefix' => 'shop'],
    function () {
        Route::get('/', 'ShopController@shop')->name('shop');
        Route::get('/order/product/{product}', 'OrderController@index')->name('order.index');
        Route::get('/order/process/{product}', 'OrderController@process')->name('order.process');
        Route::get('/order/summary/{order}', 'OrderController@summary')->name('order.summary');
        Route::get('/order/retry/{order}', 'OrderController@retry')->name('order.retry');
        Route::get('/orders', 'OrderController@list')->name('order.list');
        Route::post('/search', 'ShopController@search')->name('product.search');
    }
);

Route::group(
    ['middleware' => 'admin', 'prefix' => 'admin'],
    function () {
        Route::get('/', 'HomeController@index');
        Route::get('/users', 'UserController@index')->name('users.index');
        Route::post('/users', 'UserController@store')->name('users.store');
        Route::get('/users/{user}', 'UserController@show')->name('users.show');
        Route::put('/users/{user}', 'UserController@update')->name('users.update');

        Route::resource('product', 'ProductController');
    }
);
