<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Order extends Model
{
    protected $guarded = [];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::Class);
    }

    public function scopeFilterUser(Builder $query): Builder
    {
        return $query->where('user_id', '=', auth()->user()->id);
    }
}
