<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;

class Product extends Model
{
    protected $guarded = [];

    public function scopeDescription(Builder $query, string $description): Builder
    {
        if ($description) {
            return $query->where('description', 'LIKE', "%$description%");
        }
    }

    public function scopeStatus(Builder $query, int $status): Builder
    {
        return $query->where('status', '=', $status);
    }

    public function order(): HasOne
    {
        return $this->hasOne(Order::Class);
    }
}
