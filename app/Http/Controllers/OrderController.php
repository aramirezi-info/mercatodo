<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Order;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Repositories\Order\OrderRepository;
use Illuminate\Http\RedirectResponse;
use App\Payments\PaymentFactory;

class OrderController extends Controller
{

    protected $paymentFactory;
    protected $orderRepositorie;
    protected $defaultPaymentMethod;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OrderRepository $orderRepositorie)
    {
        $this->orderRepositorie = $orderRepositorie;
        $this->defaultPaymentMethod = config('payments.method');
        $this->paymentFactory = PaymentFactory::initialize($this->defaultPaymentMethod);
    }

    /**
     * Show the main page of the store with the search filter
     *
     * @return \Illuminate\View\View
     */
    public function index(Product $product): View
    {
        return view('order.index')->with('product', $product);
    }


    /**
     * The connection process with the payment gateway begins
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process(Request $request, int $product): RedirectResponse
    {
        $order = $this->orderRepositorie->create(
            [
            'user_id' => auth()->user()->id,
            'product_id' => $product
            ]
        );

        $response = $this->paymentFactory->transaction($request, $order->id, $order->product);

        if ($response->isSuccessful()) {
            $this->orderRepositorie->registerTransactionData($order, $response->requestId(), $response->processUrl());
            return redirect()->away($response->processUrl());
        } else {
            return back()->withError(trans('global.payment_gateway_connection_error'));
        }
    }


    /**
     * The information of a transaction is obtained
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\View\View
     */
    public function summary(Order $order): View
    {
        $response = $this->paymentFactory->query($order);

        if ($response->isSuccessful()) {
            $responseTransaction = $response->status();
            $this->orderRepositorie->updateStatusTransaction($order, $responseTransaction->status());
            $message = $responseTransaction->message();
            $order = $order->refresh();
        } else {
            $message = $response->status()->message();
        }

        return view('order.summary', compact('order', 'message'));
    }
    

    /**
     * Allows to retry failed transaction
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function retry(Request $request, Order $order): RedirectResponse
    {
        $response = $this->paymentFactory->transaction($request, $order->id, $order->product);

        if ($response->isSuccessful()) {
            $this->orderRepositorie->registerTransactionData($order, $response->requestId(), $response->processUrl());
            return redirect()->away($response->processUrl());
        } else {
            return back()->withError(trans('global.payment_gateway_connection_error'));
        }
    }


    /**
     * Get list of orders by user
     *
     * @return \Illuminate\View\View
     */
    public function list(): View
    {
        $orders = $this->orderRepositorie->paginate(10);
        return view('order.list', compact('orders'));
    }
}
