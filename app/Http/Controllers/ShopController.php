<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Order\OrderRepository;
use App\PlacetoPay\WebChekOut;
use Illuminate\Http\RedirectResponse;

class ShopController extends Controller
{
    protected $productRepositorie;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $productRepositorie)
    {
        $this->productRepositorie = $productRepositorie;
    }

    /**
     * Show the main page of the store
     *
     * @return \Illuminate\View\View
     */
    public function shop(): View
    {
        $products = $this->productRepositorie->paginate(10);
        return view('shop.products')->with('products', $products);
    }

     /**
     * Show the main page of the store with the search filter
     *
     * @return \Illuminate\View\View
     */
    public function search(Request $request): View
    {
        $products = $this->productRepositorie->search($request->all());
        return view('shop.products')->with('products', $products);
    }
}
