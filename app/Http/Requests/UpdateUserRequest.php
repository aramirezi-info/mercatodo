<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|string|unique:users,email,' . $this->get('id') . '|email|max:255',
            'password' => 'nullable',
            'status' => 'required',
        ];
    }

    /**
     * Get the error messages from the validation that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => trans('global.message_required'),
            'name.max' => trans('global.message_max'),
            'email.email' => trans('global.message_email'),
            'email.required' => trans('global.message_required'),
            'email.max' => trans('global.message_max'),
            'email.unique' => trans('global.message_email_unique'),
            'status.required' => trans('global.message_required'),
        ];
    }

     /**
     * Get the custom names of the attributes.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => trans('global.name'),
            'email' => trans('global.email'),
            'status' => trans('global.status'),
        ];
    }
}
