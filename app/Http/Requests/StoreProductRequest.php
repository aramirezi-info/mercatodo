<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'description' => 'required|max:200',
            'price' => 'required|numeric|digits_between:1,11',
            'status' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ];
    }

    /**
     * Get the error messages from the validation that apply to the request.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'description.required' => trans('global.message_required'),
            'description.max' => trans('global.message_max'),
            'price.required' => trans('global.message_required'),
            'price.max' => trans('global.message_max'),
            'image.required' => trans('global.message_required')
        ];
    }

     /**
     * Get the custom names of the attributes.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'description' => trans('global.description'),
            'price' => trans('global.price'),
            'status' => trans('global.status'),
            'image' => trans('global.image')
        ];
    }
}
