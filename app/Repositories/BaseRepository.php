<?php

namespace App\Repositories;

abstract class BaseRepository
{

    abstract public function getModel();

    public function find($id)
    {
        return $this->getModel()->find($id);
    }
    
    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function paginate(int $num)
    {
        return $this->getModel()->paginate($num);
    }

    public function create(array $data)
    {
        return $this->getModel()->create($data);
    }

    public function update(object $object, array $data)
    {
        $object->fill($data);
        $object->save();
        return $object;
    }

    public function delete(object $object)
    {
        return $object->delete();
    }
}
