<?php

namespace App\Repositories\Order;

use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Order;

class OrderRepository extends BaseRepository
{

    /**
     * Set repository model
     *
     * @return \App\Models\Order
     */
    public function getModel(): Order
    {
        return new Order();
    }

    public function registerTransactionData(Order $order, int $transactionId, string $transactionUrl): bool
    {
        $order->transaction_id = $transactionId ;
        $order->transaction_url = $transactionUrl;
        return $order->save();
    }

    public function updateStatusTransaction(Order $order, string $status): bool
    {
        $order->status = $status ;
        return $order->save();
    }

    /**
     * Get all active products paged
     *
     * @param  int  $num
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate(int $num): LengthAwarePaginator
    {
        return $this->getModel()->filterUser()->orderBy('id', 'desc')->paginate($num);
    }
}
