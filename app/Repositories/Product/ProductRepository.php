<?php

namespace App\Repositories\Product;

use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ProductRepository extends BaseRepository
{

    private const STATUS_ACTIVE = 1;

    /**
     * Set repository model
     *
     * @return \App\Models\Product
     */
    public function getModel(): Product
    {
        return new Product();
    }

    /**
     * Get all paginated products
     *
     * @param  int  $num
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getAllPaginate(int $num): LengthAwarePaginator
    {
        return $this->getModel()->paginate($num);
    }

    /**
     * Get all active products paged
     *
     * @param  int  $num
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate(int $num): LengthAwarePaginator
    {
        return $this->getModel()->status(self::STATUS_ACTIVE)->paginate($num);
    }

    /**
     * Store product information
     *
     * @param  array  $data
     * @return bool
     */
    public function create(array $data): bool
    {
        $fileName = $data['image']->hashName();
        $product = new Product();
        $product->description = $data['description'];
        $product->price = $data['price'];
        $product->status = $data['status'];
        $product->image = $data['image']->storeAs('', $fileName, 'products');
        return  $product->save();
    }

    /**
     * Update stored product information
     *
     * @param  object  $object
     * @param  array  $data
     * @return \App\Models\Product
     */
    public function update(object $object, array $data): Product
    {
        $object->description = $data['description'];
        $object->price = $data['price'];
        $object->status = $data['status'];
        if (isset($data['image'])) {
            $fileName = $data['image']->hashName();
            $object->image = $data['image']->storeAs('', $fileName, 'products');
        }
        $object->save();
        return $object;
    }

     /**
     * filter products
     *
     * @param  array  $data
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function search(array $data): LengthAwarePaginator
    {
        return $this->getModel()->description($data['search'])->status(self::STATUS_ACTIVE)->paginate(10);
    }
}
