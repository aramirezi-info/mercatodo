<?php

namespace App\Payments;

class PaymentFactory
{
    public static function initialize($type)
    {
        switch ($type) {
            case 'placetopay':
                $pay = new WebChekOutPlaceToPay();
                $pay->setConfig(
                    [
                    'url' => config('payments.placetopay.url'),
                    'login' =>  config('payments.placetopay.login'),
                    'trankey' =>  config('payments.placetopay.trankey'),
                    'currency' => config('payments.placetopay.currency'),
                    ]
                );
                return $pay;
            break;
            default:
                throw new \App\Exceptions\MethodPaymentException('Not found method payment');
            break;
        }
    }
}
