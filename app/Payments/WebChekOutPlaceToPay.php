<?php

namespace App\Payments;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Dnetix\Redirection\PlacetoPay;
use Dnetix\Redirection\Message\RedirectResponse;
use Dnetix\Redirection\Message\RedirectInformation;

class WebChekOutPlaceToPay implements PaymentInterface
{
    private $config;

    public function connection(): PlacetoPay
    {
        $config = $this->getConfig();
        $placetopay = new PlacetoPay(
            [
            'login' => $config['login'],
            'tranKey' => $config['trankey'],
            'url' => $config['url'],
            'rest' => [
                'timeout' => 45,
                'connect_timeout' => 30,
            ],
            ]
        );

        return $placetopay;
    }

    public function transaction(Request $request, int $reference, Product $product): RedirectResponse
    {
        $placetopay = $this->connection();
        $config = $this->getConfig();
        $requestPlace = [
            'payment' => [
                'reference' => $reference,
                'description' => $product->description,
                'amount' => [
                    'currency' => $config['currency'],
                    'total' => $product->price,
                ]
            ],
            'expiration' => date('c', strtotime('+7 days')),
            'returnUrl' => route('order.summary', $reference),
            'ipAddress' => $request->ip(),
            'userAgent' => $request->header('User-Agent')
        ];

        return $placetopay->request($requestPlace);
    }

    public function query(Order $order): RedirectInformation
    {
        $placetopay = $this->connection();
        return $placetopay->query($order->transaction_id);
    }

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}
