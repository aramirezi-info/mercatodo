<?php

namespace App\Payments;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;

interface PaymentInterface
{
    public function connection();
    public function transaction(Request $request, int $reference, Product $product);
    public function query(Order $order);
    public function getConfig();
    public function setConfig(array $config);
}
