@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if (count($errors) > 0)
        <div class="col-md-9">
            <div class="alert alert-danger alert-dismissible section-margin-top-50">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </h4>
            </div>
        </div>
        @endif
        <div class="col-md-9">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">@lang('global.home')</a></li>
                    <li class="breadcrumb-item"><a href="{{route('users.index')}}">@lang('user.users')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('global.edit')</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header">@lang('user.info_users')</div>

                <div class="card-body">

                    <form method="post" action="{{route('users.update',$user->id)}}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" id="id" value="{{$user->id}}">
                        <div class="form-group">
                            <label for="formGroupExampleInput">@lang('global.name')</label>
                            <input type="text" class="form-control" name="name" id="name" maxlength="255" required placeholder="@lang('global.name')" value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">@lang('global.email')</label>
                            <input type="email" class="form-control" name="email" id="email" maxlength="255" required placeholder="@lang('global.email')" value="{{$user->email}}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">@lang('global.status')</label>
                            <select class="form-control" name="status" id="status">
                                <option value="1" @if($user->status == 1) selected="selected" @endif >@lang('Activo')</option>
                                <option value="0" @if($user->status == 0) selected="selected" @endif >@lang('Inactivo')</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">@lang('global.send')</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
