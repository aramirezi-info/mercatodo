@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">@lang('global.home')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('user.users')</li>
                </ol>
            </nav>

            @if(Session::has('success'))
                 <div class="alert alert-success" role="alert">
                     {{ Session::get('success') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">@lang('user.list_users')</div>

                <div class="card-body">
                  
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">@lang('global.name')</th>
                            <th scope="col">@lang('global.email')</th>
                            <th scope="col">@lang('global.verify')</th>
                            <th scope="col">@lang('global.status')</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{($user->email_verified_at) ? 'Si': 'No' }}</td>
                                <td>@if($user->status) <span class="badge badge-success">Activo</span> @else <span class="badge badge-danger">Inactivo</span> @endif</td>
                                <td><a href="{{route('users.show', $user->id)}}">@lang('global.edit')</a></td>
                            </tr>
                        @endforeach
                      
                    </tbody>
                    </table>

                    {{ $users->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
