@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if (count($errors) > 0)
        <div class="col-md-9">
            <div class="alert alert-danger alert-dismissible section-margin-top-50">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </h4>
            </div>
        </div>
        @endif
        <div class="col-md-9">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">@lang('global.home')</a></li>
                    <li class="breadcrumb-item"><a href="{{route('product.index')}}">@lang('product.products')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('global.create')</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header">@lang('product.info_product')</div>

                <div class="card-body">

                    <form method="POST" action="{{route('product.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput">@lang('global.description')</label>
                            <input type="text" class="form-control" name="description" id="description" maxlength="200" required placeholder="@lang('global.description')">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">@lang('global.price')</label>
                            <input type="text"  pattern="[0-9]*"  min="0" class="form-control soloNumeros" name="price" id="price"maxlength="11" required placeholder="@lang('global.price')">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">@lang('global.status')</label>
                            <select class="form-control" name="status" id="status">
                                <option value="1">@lang('Activo')</option>
                                <option value="0">@lang('Inactivo')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">@lang('global.image')</label>
                            <input type="file" class="form-control-file" name="image" id="image" accept=".jpeg,.jpg,.png" placeholder="@lang('global.image')">
                        </div>
                        <button type="submit" class="btn btn-primary">@lang('global.send')</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
