@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{__('global.home')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{__('product.products')}}</li>
                </ol>
            </nav>

            @if(Session::has('success'))
                 <div class="alert alert-success" role="alert">
                     {{ Session::get('success') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">@lang('product.list_products')</div>

                <div class="card-body">
                  
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">@lang('global.image')</th>
                            <th scope="col">@lang('global.description')</th>
                            <th scope="col">@lang('global.price')</th>
                            <th scope="col">@lang('global.status')</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($products as $product)
                            <tr>
                                <td><img src="{{asset('products/'.$product->image)}}" style="max-width:200px;max-height:200px;"/></td>
                                <td>{{$product->description}}</td>
                                <td>{{$product->price}}</td>
                                <td>@if($product->status) <span class="badge badge-success">Activo</span> @else <span class="badge badge-danger">Inactivo</span> @endif</td>
                                <td><a href="{{route('product.edit', $product->id)}}">@lang('global.edit')</a></td>
                            </tr>
                        @endforeach
                      
                    </tbody>
                    </table>

                    {{ $products->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
