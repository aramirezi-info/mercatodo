@extends('layouts.master')
@section('title',trans('order.order'))
@section('body')

<div class="cart-table-area section-padding-100 section-marign-top-50">
    <div class="container-fluid">
        @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                {{ Session::get('error') }}
            </div>
        @endif
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">@lang('shop.nunberOrder')</th>
                    <th scope="col">@lang('shop.product')</th>
                    <th scope="col">@lang('shop.price')</th>
                    <th scope="col">@lang('shop.status')</th>
                    <th scope="col">@lang('shop.date')</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @forelse($orders as $order)
                    <tr>
                        <th scope="row">{{$order->id}}</th>
                        <td>{{$order->product->description}}</td>
                        <td>${{$order->product->price}}</td>
                        <td>{{trans('global.'.$order->status)}}</td>
                        <td>{{$order->created_at->format('d-m-Y')}}</td>
                        <td>
                            @if($order->status == 'PENDING')
                            <a href="{{$order->transaction_url}}" title="@lang('shop.retry')"  class="btn btn-info">
                                <i class="fa fa-retweet" aria-hidden="true"></i>
                            </button>
                            @endif
                            @if($order->status == 'REJECTED')
                            <a href="{{route('order.retry',$order->id)}}" title="@lang('shop.pay')" class="btn btn-success">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            </button>
                            @endif
                        </td>
                    </tr>
                    @empty
                        <div class="col-12 col-sm-12 col-md-12 col-xl-12">
                            <div class="alert alert-primary" role="alert">
                                @lang('global.no_results_found')
                            </div>
                        </div>
                    @endforelse

            </tbody>
        </table>
    </div>

    {{ $orders->links() }}

</div>

@stop