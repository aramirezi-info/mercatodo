@extends('layouts.master')
@section('title','Inicio')

@section('body')

    <div class="cart-table-area section-padding-100">
            <div class="container-fluid">

                <div class="alert alert-primary alert-dismissible section-margin-top-50">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>
                        <ul>
                            <li>{{$message}}</li>
                        </ul>
                    </h4>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="cart-title mt-50">
                            <h2>@lang('shop.order')</h2>
                        </div>

                        <div class="cart-table clearfix " style="min-width:100% !important;">
                            <table class="table" style="min-width:100% !important;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>@lang('global.description')</th>
                                        <th>@lang('global.price')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="cart_product_img">
                                            <a href="#"><img style="width:166px;height:166px;" src="{{asset('products/'.$order->product->image)}}" alt="Product"></a>
                                        </td>
                                        <td class="cart_product_desc">
                                            <h5>{{$order->product->description}}</h5>
                                        </td>
                                        <td class="price">
                                            <span>${{$order->product->price}}</span>
                                        </td>
                                    </tr>
                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                            <h5>@lang('shop.summary')</h5>
                            <ul class="summary-table">
                                <li><span>@lang('shop.subtotal'):</span> <span>${{$order->product->price}}</span></li>
                                <li><span>@lang('shop.delivery'):</span> <span>@lang('shop.free')</span></li>
                                <li><span>@lang('shop.total'):</span> <span>${{$order->product->price}}</span></li>
                            </ul>

                            @if($order->status != 'APPROVED')
                                <div class="cart-btn mt-100">
                                    <a href="{{route('order.retry', $order->id)}}" class="btn amado-btn w-100">@lang('shop.retry')</a>
                                </div>
                             @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>

@stop