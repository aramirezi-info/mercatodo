@extends('layouts.master')
@section('title','Inicio')

@section('body')

    <div class="cart-table-area section-padding-100">
            <div class="container-fluid">


                @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="cart-title mt-50">
                            <h2>@lang('shop.order')</h2>
                        </div>

                        <div class="cart-table clearfix " style="min-width:100% !important;">
                            <table class="table" style="min-width:100% !important;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>@lang('global.description')</th>
                                        <th>@lang('global.price')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="cart_product_img">
                                            <a href="#"><img style="width:166px;height:166px;" src="{{asset('products/'.$product->image)}}" alt="Product"></a>
                                        </td>
                                        <td class="cart_product_desc">
                                            <h5>{{$product->description}}</h5>
                                        </td>
                                        <td class="price">
                                            <span>${{$product->price}}</span>
                                        </td>
                                    </tr>
                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                            <h5>@lang('shop.summary')</h5>
                            <ul class="summary-table">
                                <li><span>@lang('shop.subtotal'):</span> <span>${{$product->price}}</span></li>
                                <li><span>@lang('shop.delivery'):</span> <span>@lang('shop.free')</span></li>
                                <li><span>@lang('shop.total'):</span> <span>${{$product->price}}</span></li>
                            </ul>
                            <div class="cart-btn mt-100">
                                <a href="{{route('order.process',$product->id)}}" class="btn amado-btn w-100">@lang('shop.pay')</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@stop