<?php

return [

    'order' => 'Orden de compra',
    'summary' => 'Resumen',
    'subtotal' => 'subtotal',
    'delivery' => 'Envio',
    'total' => 'Total',
    'pay' => 'Pagar',
    'retry' => 'Reintentar Pago',
    'nunberOrder' => 'No. Orden',
    'status' => 'Estado',
    'date' => 'Fecha',
    'product' => 'Producto',
    'price' => 'Precio',
    'free' => 'Gratis'
];