<?php

return [

    'users' => 'Usuarios',
    'list_users' => 'Listado de usuarios',
    'info_users' => 'Información usuario',
    'user_successfully_edited' => 'Usuario modificado con éxito',

];
