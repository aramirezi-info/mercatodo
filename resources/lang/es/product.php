<?php

return [

    'products' => 'Producto',
    'list_products' => 'Listado de productos',
    'info_product' => 'Información producto',
    'product_successfully_created' => 'Producto creado con éxito',
    'product_successfully_edited' => 'Producto modificado con éxito',
    'product_not_found' => 'No se encontraron productos',

];
