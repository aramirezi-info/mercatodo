<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Iniciar sesión',
    'remember' => 'Recuerdame',
    'register' => 'Registrarse',
    'forgot_password' => '¿Olvidaste tu contraseña?',
    'reset_password' => 'Restablecer la contraseña',
    'link_reset' => 'Enviar enlace de restablecimiento de contraseña',
    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',

];
