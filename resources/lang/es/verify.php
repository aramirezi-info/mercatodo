<?php

return [

    'header' => 'Verifique su dirección de correo electrónico',
    'messages' => 'Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.',
    'content' => 'Antes de continuar, consulte su correo electrónico para ver si hay un enlace de verificación.',
    'sub_content' => 'Si no recibió el correo electrónico',
    'link' => 'haga clic aquí para solicitar otro'



];
