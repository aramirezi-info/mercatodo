<?php

return [

    'method' => env('PAYMENT_GATEWAY', ''),
    'placetopay' => [
        'url' => env('PLACETOPAY_URL', ''),
        'login' => env('PLACETOPAY_LOGIN', ''),
        'trankey' => env('PLACETOPAY_TRANKEY', ''),
        'currency' => env('PLACETOPAY_CURRENCY', 'COP'),
    ]
    
];
