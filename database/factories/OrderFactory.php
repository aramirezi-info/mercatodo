<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'product_id' => function() {
            return factory(Product::class)->create()->id;
        },
        'transaction_id' => $faker->randomNumber(),
        'status' => $faker->randomElement($array = array ('CREATED','APPROVED','PAYED','REJECTED','PENDING'))
    ];
});
