<?php

namespace Tests\Feature\Order;

use App\Models\Product;
use App\Models\Order;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateOrderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function create_order()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $product = factory(Product::class)->create();

        $this->get(route('order.process', $product->id))->assertRedirect();
    
        $order = Order::first();
        $this->assertEquals( $user->id, $order->user_id);
        $this->assertEquals( $product->id, $order->product_id);
    }
}
