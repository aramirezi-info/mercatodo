<?php

namespace Tests\Feature\Order;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Order;

class ShowOrderTest extends TestCase
{
    use RefreshDatabase; 

    /** @test */
    public function it_load_show_order()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $order = factory(Order::class)->create();

        $response = $this->get(route('order.summary',$order->id));

        $response->assertStatus(200);
    }

    /** @test */
    public function it_show_order()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $order = factory(Order::class)->create();

        $response = $this->get(route('order.summary',$order->id));

        $response->assertOk();
        $order = Order::first();
        $response->assertViewIs('order.summary');
    }    
}
