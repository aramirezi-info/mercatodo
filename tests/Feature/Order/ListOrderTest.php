<?php

namespace Tests\Feature\Order;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Order;

class ListOrderTest extends TestCase
{
    use RefreshDatabase; 

    /** @test */
    public function it_visit_list_orders()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('order.list'));

        $response->assertStatus(200);
    }

    /** @test */
    public function it_list_orders()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $this->actingAs($user);
        factory(Order::class,4)->create();
 
        $response = $this->get(route('order.list'));

        $response->assertOk();
        $orders = Order::all();
        $response->assertViewIs('order.list');
    }
}
