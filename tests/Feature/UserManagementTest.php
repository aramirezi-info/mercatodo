<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserManagementTest extends TestCase
{
    use RefreshDatabase;

    private string $routeIndex = 'users.index';

     /** @test */
     public function list_of_users_can_be_retrieved()
     {
        $this->userLogin = factory(User::class)->create();
        $this->actingAs($this->userLogin);

        $response = $this->get(route($this->routeIndex));

        $response->assertOk();
        $response->assertViewIs('user.index');
        $response->assertViewHas('users');
     }

    /** @test */
    public function a_user_can_be_retrieved()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('users.show',$user->id));

        $response->assertOk();
        $user = User::first();
        $response->assertViewIs('user.show');
        $response->assertViewHas('user', $user);
    }


     /** @test */
     public function a_user_can_be_created()
     {
         $user = factory(User::class)->create();
         $this->actingAs($user);

         $response = $this->post(route('users.store'),[
             'name' => 'alejandro',
             'email' => 'aramirezi.info@gmail.com',
             'password' => '$2y$10$C1frpuRgzLJneWm.vbvTc.lwavIh4IK/TSuI1S0B0zNDDJfwUPqhe',
             'email_verified_at' => '2020-12-29 04:32:31',

         ]);
        
         $this->assertCount(2, User::all());
         $user = User::orderBy('id', 'desc')->first();
         $this->assertEquals($user->name, 'alejandro');
         $this->assertEquals($user->email, 'aramirezi.info@gmail.com');
         $this->assertEquals($user->password, '$2y$10$C1frpuRgzLJneWm.vbvTc.lwavIh4IK/TSuI1S0B0zNDDJfwUPqhe');
         $this->assertEquals($user->email_verified_at, '2020-12-29 04:32:31');
         $response->assertRedirect(route($this->routeIndex));
     }

     /** @test */
     public function a_user_can_be_updated()
     {
         $user = factory(User::class)->create();
         $this->actingAs($user);
     
         $response = $this->put(route('users.update',$user->id),[
             'name' => 'alejandro',
             'email' => 'aramirezi.info@gmail.com',
             'password' => '$2y$10$C1frpuRgzLJneWm.vbvTc.lwavIh4IK/TSuI1S0B0zNDDJfwUPqhe',
             'status' => 1,
         ]);

         $this->assertCount(1, User::all());
         $user = $user->fresh();
         $this->assertEquals($user->name, 'alejandro');
         $this->assertEquals($user->email, 'aramirezi.info@gmail.com');
         $this->assertEquals($user->password, '$2y$10$C1frpuRgzLJneWm.vbvTc.lwavIh4IK/TSuI1S0B0zNDDJfwUPqhe');
         $response->assertRedirect(route($this->routeIndex));
     }
}


   