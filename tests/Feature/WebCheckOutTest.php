<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Product;
use App\Models\Order;
use App\Models\User;
use App\Payments\WebChekOutPlaceToPay;

class WebCheckOutTest extends TestCase
{
    use RefreshDatabase; 
    
    /** @test */
    public function verify_webchekout_connection()
    {

        $place = new WebChekOutPlaceToPay();
        $place->setConfig([
            'url' => config('payments.placetopay.url'),
            'login' =>  config('payments.placetopay.login'),
            'trankey' =>  config('payments.placetopay.trankey')
        ]);
        $place->connection();

        $this->assertEquals('App\Payments\WebChekOutPlaceToPay', get_class($place));

    }

    /** @test */
    public function verify_webchekout_transaction()
    {

        $user = factory(User::class)->create();
        $this->actingAs($user);

        $place = new WebChekOutPlaceToPay();
        $place->setConfig([
            'url' => config('payments.placetopay.url'),
            'login' =>  config('payments.placetopay.login'),
            'trankey' =>  config('payments.placetopay.trankey'),
            'currency' => config('payments.placetopay.currency')
        ]);
        $place->connection();

        $request = request();
        $order = factory(Order::class)->create();

        $trans = $place->transaction($request,$order->id, $order->product);

        $this->assertTrue($trans->isSuccessful());

       }
}
