<?php

namespace Tests\Feature;

use \Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;

class ProductManagementTest extends TestCase
{
    use RefreshDatabase;

    private string $routeIndex = 'product.index';

    /** @test */
    public function list_of_product_can_be_retrieved()
    {
        
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route($this->routeIndex));

        $response->assertOk();
        $response->assertViewIs('product.index');
        $response->assertViewHas('products');
    
    }

    /** @test */
    public function a_product_can_be_retrieved()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $product = factory(Product::class)->create();

        $response = $this->get(route('product.edit',$product->id));

        $response->assertOk();
        $product = Product::first();
        $response->assertViewIs('product.edit');
        $response->assertViewHas('product', $product);
    }

    /** @test */
    public function a_product_can_be_created()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $file = UploadedFile::fake()->image('img/product-img/product1.jpg', 600, 600);
        $hashName = $file->hashName();

        $response = $this->post(route('product.store'),[
            'description' => 'Silla Gales Blanca',
            'image' => $file,
            'status' => 1,
            'price' => '200000'
        ]);
       
        $this->assertCount(1, Product::all());
        $product = Product::first();
        $this->assertEquals($product->description, 'Silla Gales Blanca');
        $this->assertEquals($product->image, $hashName);
        $this->assertEquals($product->status, '1');
        $this->assertEquals($product->price, '200000.0');
        $response->assertRedirect(route($this->routeIndex));
    }

    /** @test */
    public function a_product_can_be_updated()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $product = factory(Product::class)->create();
        $file = UploadedFile::fake()->image('img/product-img/product1.jpg', 600, 600);
        $hashName = $file->hashName();

        $response = $this->put(route('product.update',$product->id),[
            'description' => 'Silla Gales Blanca',
            'image' => $file,
            'status' => 1,
            'price' => '200000'
        ]);

        $this->assertCount(1, User::all());
        $product = $product->fresh();
        $this->assertEquals($product->description, 'Silla Gales Blanca');
        $this->assertEquals($product->image, $hashName);
        $this->assertEquals($product->status, '1');
        $this->assertEquals($product->price, '200000.0');
        $response->assertRedirect(route($this->routeIndex));

    }
}
